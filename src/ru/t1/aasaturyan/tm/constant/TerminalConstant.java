package ru.t1.aasaturyan.tm.constant;

public final class TerminalConstant {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    private TerminalConstant() {
    }

}
