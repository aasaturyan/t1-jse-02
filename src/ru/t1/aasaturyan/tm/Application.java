package ru.t1.aasaturyan.tm;

import static ru.t1.aasaturyan.tm.constant.TerminalConstant.*;

public final class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(String[] args) {
        if (args == null || args.length == 0) {
            System.out.println("Required parameter not passed..");
            showHelp();
        } else {
            final String argument = args[0];
            processArgument(argument);
        }
    }

    public static void processArgument(String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case HELP:
                showHelp();
                break;
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            default:
                showError();
                break;
        }
    }

    public static void showError() {
        System.out.println("[ERROR]");
        System.out.println("This argument is not supported...");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Artyom Asaturyan");
        System.out.println("email: aasaturyan@tech-code.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info.\n", ABOUT);
        System.out.printf("%s - Show application version.\n", VERSION);
        System.out.printf("%s - Show command list.\n", HELP);
    }

}
